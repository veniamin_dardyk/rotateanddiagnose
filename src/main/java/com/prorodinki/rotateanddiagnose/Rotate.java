package com.prorodinki.rotateanddiagnose;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class Rotate {

    public static void rotate(Path imagePath, int[] diffDiagsCounter) {
        BufferedImage master;

        JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
        jpegParams.setCompressionMode(JPEGImageWriteParam.MODE_EXPLICIT);
        jpegParams.setCompressionQuality(1f);

        NeuralNetwork nn = new NeuralNetwork();

        try {
            boolean renamed=true;
            String imageName = imagePath.toString();
            master = ImageIO.read(new File(imageName));
//            for (int angle = 0; angle<=270; angle+=90){
//                BufferedImage rotated;
//                rotated = rotateImageByDegrees(master, angle);
//                writeRotated(nn, imageName, rotated, angle, jpegParams);
//            }
            int [] angles={0,180,360};
            int[] diags = new int[3];
            for (int i=0; i<3; i++){
                BufferedImage rotated = master;
                int resultAngle = angles[i];
                int currentAngle=0;
                while(currentAngle < resultAngle) {
                    rotated = rotateImageByDegrees(rotated, 180);
                    currentAngle +=180;
                }
                String rotatedName = writeRotated(imageName, rotated, resultAngle, jpegParams);
                diags[i] = diagnose(nn, rotatedName).ordinal();
            }
            if (diags[0] != diags[1]){
                diffDiagsCounter[0]++;
                System.out.println("0 != 180 => Всего: "+ diffDiagsCounter[0]);
            }
            if (diags[0] != diags[2]){
                diffDiagsCounter[1]++;
                System.out.println("0 != 360 => Всего: "+ diffDiagsCounter[1]);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
    public static String  writeRotated(String imageName, BufferedImage rotated, int angle, JPEGImageWriteParam jpegParams) throws IOException {
        String rotatedName = imageName.replace(".jpg", "_"+ (angle==360? "2x180": Integer.toString(angle)) +".jpg");
        File outputfile = new File(rotatedName);
        //            ImageIO.write(rotated, "jpg", outputfile);
        final ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        // specifies where the jpg image has to be written
        FileImageOutputStream fileImageOutputStream = new FileImageOutputStream(outputfile);
        writer.setOutput(fileImageOutputStream);
        // writes the file with given compression level
        // from your JPEGImageWriteParam instance
        writer.write(null, new IIOImage(rotated, null, null), jpegParams);
        fileImageOutputStream.close();
        return rotatedName;
    }

    public static Diagnosis diagnose(NeuralNetwork nn, String rotatedName){
        File outputfile = new File(rotatedName);
        Diagnosis diag = nn.getDiagnoseGroup(rotatedName);
        File finalFile = new File(rotatedName.replace(".jpg", "_"+ diag.name() +".jpg"));
        boolean renamed = outputfile.renameTo(finalFile);
        System.out.println(finalFile.getAbsolutePath() + " "+renamed);
        return diag;

    }

    public static BufferedImage rotateImageByDegrees(BufferedImage img, double angle) {

        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
        int w = img.getWidth();
        int h = img.getHeight();
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = rotated.createGraphics();
        AffineTransform at = new AffineTransform();
        at.translate((newWidth - w) / 2, (newHeight - h) / 2);

        int x = w / 2;
        int y = h / 2;

        at.rotate(rads, x, y);
        g2d.setTransform(at);
        g2d.drawImage(img, 0, 0, null);
        g2d.dispose();

        return rotated;
    }

}
