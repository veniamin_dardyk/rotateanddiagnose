package com.prorodinki.rotateanddiagnose;

public enum Severity {
    AWAITING,
    SUPERVISION,
    ROUTINE,
    EMERGENCY,
    REPEAT
}

