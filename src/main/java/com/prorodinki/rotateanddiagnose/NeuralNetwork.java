package com.prorodinki.rotateanddiagnose;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;


public class NeuralNetwork {

    Resource resource;
    String jsonCaseFactors;
    String neuralNetworkAPIKey = "c26a3d8f078526ded025258d38fb0501fccea6d43869c40d16" ;
    String neuralNetworkUrl ="http://35.231.66.154:8000/";
    String neuralGetDiagUrl = "api/predict_image/";

    public NeuralNetwork(){
        CaseFactors caseFactors = new CaseFactors();
        caseFactors.setAge(45);
        caseFactors.setBody_part(3);
        caseFactors.setSex(0);
        caseFactors.setSkin_type(1);
        caseFactors.setSunburn(1);
        try {
            jsonCaseFactors = new ObjectMapper().writeValueAsString(caseFactors);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Diagnosis getDiagnoseGroup( String filePath) {

        double diagGroupEval=1.0;
        double diagMelanomaEval=1.0;
        double diagBccEval=1.0;

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();

        File imageFile = new File(filePath);
        try {
            resource = new UrlResource(imageFile.toURI());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        bodyMap.add("fileD", resource);
        bodyMap.add("case_id", Integer.toString(10001));
        bodyMap.add("photo_id", "image_rotate");
        bodyMap.add("factors", jsonCaseFactors);
        bodyMap.add("key", neuralNetworkAPIKey);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(neuralNetworkUrl+neuralGetDiagUrl,
                HttpMethod.POST, requestEntity, String.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            String body = response.getBody();
            try {
                JSONObject jsonObject = new JSONObject(body);
                int status = jsonObject.getInt("status");
                Double resultGroup = Double.parseDouble(jsonObject.getString("result"));
                Double resultMelanoma = Double.parseDouble(jsonObject.getString("result_m"));
                Double resultBcc = Double.parseDouble(jsonObject.getString("result_bcc"));
                diagGroupEval = (diagGroupEval > resultGroup) ? resultGroup : diagGroupEval;
                diagMelanomaEval = (diagMelanomaEval > resultMelanoma) ? resultMelanoma : diagMelanomaEval;
                diagBccEval = (diagBccEval > resultBcc) ? resultBcc : diagBccEval;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       return getNeuroDiagnosis(diagMelanomaEval, diagBccEval);
    }

    private Diagnosis getNeuroDiagnosis(double diagMelanomaEval, double diagBccEval) {
        if (diagMelanomaEval <= 0.5 && diagMelanomaEval <= diagBccEval) return Diagnosis.MELANOMA;
        if (diagBccEval <= 0.5 && diagBccEval < diagMelanomaEval) return Diagnosis.BKRK;
        else return Diagnosis.NOONCOLOGY;
    }



    @Data
    private class CaseFactors{
        String version ="1.0";
        Integer age;
        Integer sex;
        Integer skin_type;
        Integer body_part;
        Integer sunburn;

        public CaseFactors() {
        }
    }

}
