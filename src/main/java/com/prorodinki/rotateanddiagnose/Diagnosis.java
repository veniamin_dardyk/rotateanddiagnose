package com.prorodinki.rotateanddiagnose;

public enum Diagnosis {
    UNDEFINED("Не определен", Severity.AWAITING),
    NEVUS("Невус", Severity.SUPERVISION),
    SK("Себорейная кератома", Severity.SUPERVISION),
    BKRK("Базально клеточный рак", Severity.ROUTINE),
    MELANOMA("Меланома", Severity.EMERGENCY),
    ONCOLOGY("Онкология", Severity.EMERGENCY),
    NOONCOLOGY("Патологии не обнаружено", Severity.SUPERVISION),
    UNRECOGNIZED("Снимок не распознан", Severity.REPEAT),
    HEMANGIOMA("Гемангиома", Severity.SUPERVISION),
    NOT_A_NEOPLASM("Не новообразование", Severity.SUPERVISION);


    private String diagName;
    private Severity severity;

    Diagnosis(String diagName, Severity severity) {
        this.diagName = diagName;
        this.severity = severity;
    }

    public String getDiagName() {
        return diagName;
    }

    public Severity getSeverity() {
        return severity;
    }
}

