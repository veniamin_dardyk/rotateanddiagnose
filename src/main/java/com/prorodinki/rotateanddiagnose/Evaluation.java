package com.prorodinki.rotateanddiagnose;

import nu.pattern.OpenCV;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Evaluation {

    public static void evaluateBlur(Path imagePath) {
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        OpenCV.loadShared();
        Mat img = Imgcodecs.imread(imagePath.toString());
        if (img.empty()) {
            System.out.println("Не удалось загрузить изображение - " + imagePath.toString());
        }
        Mat matLaplasian = new Mat();
        Mat matGray=new Mat();
        Imgproc.cvtColor(img, matGray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Laplacian(matGray, matLaplasian, CvType.CV_64F);
        MatOfDouble median = new MatOfDouble();
        MatOfDouble std= new MatOfDouble();
        Core.meanStdDev(matLaplasian, median , std);
        double var= Math.pow(std.get(0,0)[0],2);
        try {
            renameEvaluated(imagePath,var);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void  renameEvaluated(Path imagePath, Double var) throws IOException {
        Path targetPath = Paths.get(newName (imagePath,var));
        Files.move(imagePath, targetPath);
    }

    private static String newName(Path imagePath, Double var) {
        String fileName = imagePath.getFileName().toString();
        return "D:\\Prorodinki\\evaluated64\\"+var.toString() +"_" +fileName;
        //return imagePath.toString().replace(".jpg", "_"+ var.toString() +".jpg");
    }

}
