package com.prorodinki.rotateanddiagnose;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@SpringBootApplication
public class RotateanddiagnoseApplication  implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(RotateanddiagnoseApplication.class, args);
    }
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Rotate Application started");
        int[] diffDiagsCounter = {0,0};
        try (Stream<Path> paths = Files.walk(Paths.get("D:\\Prorodinki\\toEvaluate"))) {
            paths
                    .filter(Files::isRegularFile)
//                    .forEach(path -> Rotate.rotate(path, diffDiagsCounter));
                    .forEach(Evaluation::evaluateBlur);
        } finally {
//            System.out.println("0 != 180 => Всего: "+ diffDiagsCounter[0]);
//            System.out.println("0 != 360 => Всего: "+ diffDiagsCounter[1]);
//            System.out.println("Rotate Application finished");
        }
    }

}
